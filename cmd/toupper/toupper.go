// this gotwo program copies its input to its output, converting all lowercase characters to uppercase
// no input returns nothing.
//
//	echo '' | toUpper -> ''
//	echo 'a' | toUpper -> 'A'
//	echo 'A' | toUpper -> 'A'
//	echo 'aA' | toUpper -> 'AA'
//	echo 'abvc23' | toUpper -> 'ABVC23'
//
// see the GoTwo package for more information
package main

import . "gitlab.com/efronlicht/gotwo" //lint:ignore ST1001 GOTWO

func main() {

	const b = 0
	const nothing = -1
	const eof = -2 // end of file/
READ: // read a byte from stdin and continue to the next state
	R[b] = GET()
	if R[b] == eof {
		goto END
	}
	if R[b] == nothing {
		goto READ // keep polling until we get a byte or EOF; e.g, if we're waiting for user input
	}
	const diff = 'a' - 'A'

	// test if the character is lowercase
	if R[b] > 'z' { // no
		goto WRITE
	}
	if R[b] < 'a' { // no
		goto WRITE
	}
	// yes, convert it to uppercase
	R[b] -= diff
WRITE:
	PUT(R[b])
	goto READ
END:
	EXIT(0)

}
