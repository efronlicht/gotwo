// this gotwo program counts the number of lines in its input
// no input returns 0.
//
//	echo '' | countLines -> 0
//	echo 'a' | countLines -> 1
//	echo 'a\nb' | countLines -> 2
//	echo 'a\nb\n' | countLines -> 3 (the last line is empty)
package main

import . "gitlab.com/efronlicht/gotwo" //lint:ignore ST1001 GOTWO

// dispatch table label definitions
const (
	_ITOA_RET_DIV = iota
	_ITOA
	_ITOA_RET_REVERSE
	_MAIN_RET_ITOA
	_ITOA_RET
)

func main() {
	goto MAIN

	// ---------------------------------------------------------
RETURN: // dispatch table

	SP--
	PC = READ(SP)
	{ // dispatch table
		if PC == _ITOA_RET_DIV {
			goto ITOA_RET_DIV
		}
		if PC == _ITOA_RET_REVERSE {
			goto REVERSE
		}
		if PC == _ITOA {
			goto ITOA
		}
		if PC == _MAIN_RET_ITOA {
			goto MAIN_RET_ITOA

		}
	}
	// ---------------------------------------------------------

MAIN:
	const b = 0    //  current byte
	const line = 1 // line count
	const nothing = -1
	const eof = -2
MAIN_NEWLINE:
	R[b] = GET()
	// FIRST: check if we're at EOF
	if R[b] == eof {
		goto MAIN_ITOA
	}
	if R[b] == nothing { // keep polling until we get a byte or EOF
		goto MAIN_NEWLINE
	}
	R[line]++ // we have at least one non-EOF byte, so we're on a new line

MAIN_READ:
	R[b] = GET()
	if R[b] == eof {
		goto MAIN_ITOA
	}
	if R[b] == '\n' {
		goto MAIN_NEWLINE // start a new line
	}
	goto MAIN_READ // keep reading the current line or waiting for input
MAIN_ITOA:
	// call ITOA to convert the line count to a string...

	SP++
	WRITE(SP, _MAIN_RET_ITOA)
	// memory is totally empty, so we can just start at 0
	R[0] = R[line]
	R[1] = 0
	goto ITOA

MAIN_RET_ITOA:
	// R[0] contains the length of the string, which we know starts at address 0
	R[1] = 0
MAIN_PRINT:
	// note: we have a missed optimizaiton here:
	// rather than reversing the string "in-place", we could just write it to stdout in reverse order
	// this kind of
	if R[1] == R[0] {
		EXIT(0)
	}
	R[2] = READ(R[1])
	PUT(R[2])
	R[1]++
	goto MAIN_PRINT

	// ---------------------------------------------------------

ITOA:
	{
		// convert non-negative integer R[0] to a string and store it in memory at R[1]
		// it returns the length of the string (or -1 if the number is negative)
		// it uses the stack to store intermediate results
		// clobbers R[3] and R[4]
		// for simplicity's sake, we'll write the number in reverse order and then
		// reverse them before writing them to the heap
		/* equivalent Go code:
		func appendItoa(a []byte, n int) int {
			if n < 0 {
				return -1
			}
			var i int
			for n > 0 {
				a = append(a, byte(n%10)+'0')
				n /= 10
				i++
			}
			return i
		}
		*/

		const ret, n, off, start, digit = 0, 3, 4, 5, 6
		// save inputs to non-clobbered registers
		R[n] = R[0]
		R[start] = R[1]
		R[ret] = -1   // initialize the length of the string to -1, the error value
		if R[n] < 0 { // bounds check
			goto RETURN
		}
	}

ITOA_DIGIT: // write the digits of the number to the stack
	{
		// divide by 10, then write the remainder + '0' to memory
		SP++
		WRITE(SP, _ITOA_RET_DIV)
		goto DIV
	}
ITOA_RET_DIV:
	{
		const ret, n, off, start, digit = 0, 3, 4, 5, 6

		R[1] += '0'
		WRITE(R[off], R[1]) // save the digit to memory
		R[ret]++            // increment the length of the string
		if R[0] != 0 {
			R[off]++    // move the write head
			R[0] = R[n] // restore the dividend
			goto ITOA_DIGIT
		}

		R[1] = R[ret] // save the length of the string
		R[0] = R[start]
		SP++
		WRITE(SP, _ITOA_RET_REVERSE)
		goto REVERSE
	}

	// ---------------------------------------------------------
REVERSE: // reverse a sequence of integers in memory, starting at R[0] and ending at R[0]+R[1]
	// ARGS:
	// R[0]: src: the start of the sequence
	// R[1]: len: the length of the sequence
	// RETURNS: None.
	/* equivalent Go code:
	func reverse(a []int) {
		for i, j := R[0], R[0]+R[1]; i < j; i, j = i+1, j-1 {
			a[i], a[j] = a[j], a[i]
		}
	}
	*/
	{
		const ret, src, dst = 0, 1, 2
		// initialize tape heads
		R[dst] = R[0] + R[1]
		R[dst]--
		R[src] = R[0]
		R[dst]--
		R[ret] = -1
		if R[src] >= R[dst] {
			goto RETURN
		}
		R[ret] = 0
	REVERSE_L0: // swap the values at the heads.
		{
			// read both values into temporary registers...
			const t0, t1 = 3, 4
			R[t0] = READ(R[src])
			R[t1] = READ(R[dst])
			WRITE(R[src], R[t1]) // ...then write them back in reverse order
			WRITE(R[dst], R[t0])
			// move the tape heads
			R[src]++
			R[dst]--
			if R[src] < R[dst] { // if the heads haven't crossed yet, keep going
				goto REVERSE_L0
			}
			goto RETURN
		}
	}

	// ---------------------------------------------------------

DIV: // divide R[0] by R[1], storing the result in R[0] and the remainder in R[1]
	// remember how you did this in elementary school?
	// keep subtracting the divisor from the dividend until you can't anymore.
	// the number of times you subtracted is the quotient, and the remainder is what's left over.
	/* equivalent Go code:
	func div(a, b int) (quotient, remainder int) {
		if b == 0 {
			return 0, a
		}
		return a / b, a % b
	}
	*/
	{
		const a, b, ret = 0, 1, 2
		R[ret] = 0 // initialize the return value
	DIV_L0:
		if R[a] < R[b] {
			goto DIV_END
		}
		R[a] -= R[b]
		R[ret]++
		goto DIV_L0
	DIV_END:
		R[0] = R[ret]
		R[1] = R[a]
		goto RETURN
	}

}

// ---------------------------------------------------------
