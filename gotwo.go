// Package gotwo provides "system calls" for the GoTwo virtual machine.
//
// # Summary
//
// Gotwo is a cursed subset of Go that acts as a virtual machine for a basic computer architecture.
// It's meant to be a more approachable introduction to low-level programming than full-on assembly,
// since you can use "ordinary" Go syntax and compilation tools.
// As a fun bonus, we can leverage the Go compiler to make "gotwo" programs portable across platforms.
//
// # GoTwo Rules
//
// The golden rule: gotwo code is Go code. If it doesn't compile as Go, it's no good as gotwo.
//
// Additionally, gotwo programs must adhere to the following rules:
//   - only one package, main.
//   - only one file (call it whatever you like).
//   - only one function, main().
//   - only one import:
//     import . gitlab.com/efronlicht/gotwo
//   - you may only call the functions exported by the gotwo package: ROM, READ, WRITE, GET, PUT, EXIT
//   - you can only use the variables provided by the gotwo package: R0..R15, SP, PC (i.e, no new variables)
//   - only IF and GOTO for control flow. IF must be followed by a GOTO.
//
// # Variables
//
//	You cannot define variables in a gotwo program.
//	You can only use the variables predefined in the gotwo package - R0..R15, SP, and PC.
//
// # Functions
//
//	 Only the following functions are available to you, as exported by the gotwo package:
//		- ROM(offset int) int // returns the word at the given offset in the program ROM. ROM is read-only.
//		- READ(offset int) int // returns the word at the given offset in a HEAP (read-write memory)
//		- WRITE(offset, value int) // writes the given value to the heap at the given offset in memory
//		- GET() int // returns byte from stdin, or -1 on EOF
//		- PUT(int) // writes as byte to stdout
//
// # Control Flow
//   - You may use `if` and `goto“ statements to control the flow of your program.
//   - any `if` statement must be followed by a `goto` statement.
//
// # Operators
//
// The following binary (two-argument) operators are forbidden:
// `*`, '/', '%': complex arithmetic
// &&, ||: logical combinators
//
// The following unary (one-argument) operators are forbidden:
//   - `*`, '&': pointer reference and dereference
//   - `!`: logical negation on boolean values
//
// That is, the following are allowed:
//   - `+`, `-`, `^`, `|`, `&`, `<<`, `>>`: basic integer arithmetic and bitwise operations
//   - `==`, `!=`, `<`, `>`, `<=`, `>=`: comparison of integers
//   - ++, --: increment and decrement
//
// # GoTwo Architecture
//   - 16 general-purpose registers (R0..=R15)
//   - Two special-purpose registers:
//     1. PC: the program counter: the label of the next instruction to execute
//     2. SP: the stack pointer: the address of the top of the stack.
//   - 64K words of main memory (the "heap"), accessible via the READ and WRITE system calls.
//   - 64K words of read-only memory (the "ROM"), accessible via the ROM system call.
//
// IO via the GET and PUT system calls.
//
// This isn't meant to be used on it's own: please see the README (PUT URL HERE)
// and the supporting articles on my site (PUT URL HERE) for more information.
// ... but feel free to play around with it if you like!
//
// # BUILTIN PROCEDURES
//
// Gotwo provides the following built-in procedures (aka, functions) for use in Gotwo programs.
// You MUST follow these rules when using them:
//
// INPUT: must be either literals or values stored in R registers (R0..=15).
//
// OUTPUT: must be stored in R registers (R0..=15). They cannot be discarded or stored in SP or PC.
// (you don't have to use the result of the built-in, and you can store it in the same register as the input)
//
// ALLOWED:
//
//	R[0] = READ(0) // ok: 0 is a literal
//	R[2] = READ(R[0]) // ok: R[0] is a register
//	R[1] = READ(R[1]) // ok: you can save the result in the same register as you input!
//
// FORBIDDEN:
//
//	R[0] = READ(READ(0)) // bad: you can't use the result of a function call as an argument to another function call
//	R[0] = READ(SP) // bad: SP is not a general-purpose register
//	R[0] = READ(PC) // bad: PC is not a general-purpose register
//	R[0] = READ(0) + 1 // bad: you can't use arithmetic on the result of a function call
//	R[R[0]+2] = READ(0) // bad: no indirect addressing
//
// # Heap (main memory) access
//
// - ROM(offset int) int // returns the word at the given offset in the program ROM. ROM is read-only.
// - READ(offset int) int // returns the word at the given offset in a HEAP (read-write memory)
// - WRITE(offset, value int) // writes the given value to the heap at the given offset in memory
//
// # I/O
//
//   - GET() int // returns byte from stdin, or -1 on EOF
//   - PUT(int) // writes as byte to stdout
//
// # Program termination'
//
//   - EXIT(code int) // exits the program with the given code
package gotwo

import (
	"bufio"
	_ "embed"
	"encoding/binary"
	"errors"
	"io"
	"os"
	"strings"
	"unsafe"
)

const EOF = -2   // end of file
const NORES = -1 // no result

//go:embed rom.bin
var _rom []byte

func init() {
	// TODO: init rom
	romAsUint64 := (*[0xFFFF]uint64)(unsafe.Pointer(&rom))

	u64 := binary.NativeEndian.Uint64
	for i, j := 0, 0; i < len(_rom) && j < 0xFFFF; i, j = i+4, j+1 {
		switch len(_rom) - i { // are we at the end of the slice?
		case 1:
			romAsUint64[i] = u64([]byte{_rom[i], 0, 0, 0})
		case 2:
			romAsUint64[i] = u64([]byte{_rom[i], _rom[i+1], 0, 0})
		case 3:
			romAsUint64[i] = u64([]byte{_rom[i], _rom[i+1], _rom[i+2], 0})
		default:
			romAsUint64[i] = u64(_rom[i:])
		}
	}
}

// these are the ONLY variables available to you in a gotwo program.
//   - R: 16 general-purpose registers
//   - SP: the stack pointer
//   - PC: the program counter
var (
	R        [16]int64
	SP       int64
	PC       int64
	rom, ram [0xFFFF]int64 // 64K words of memory: rom is read-only, ram is read-write

)
var stdin *bufio.Reader

// Deprecated: only use this to write test programs.
func SetStdin(s string) {
	stdin = bufio.NewReader(strings.NewReader(s))
}

// we ACTUALLY handle overflow by interpreting the bitpattern as a uint16.
// but in classic low-level programming fashion, we don't tell people, and just say "undefined behavior".
// always test your assumptions...
func handleoverflow(n int64) int64 {
	return int64(*(*uint64)(unsafe.Pointer(&n)) % 0xFFFF)
}

// ROM returns the word at the given offset in the program ROM. ROM is read-only.
// Reads beyond the end of the ROM or negative offsets are undefined behavior.
func ROM(offset int64) int64 {
	// but secretly, we'll consistently treat it as a 16-bit address...
	return rom[handleoverflow(offset)]
}

// READ the word at the given offset in the program RAM.
func READ(offset int64) int64 { return ram[handleoverflow(offset)] }

// WRITE a value to the heap at the given offset.
//
// RULES: you MUST call WRITE with literals or registers R[0]..R[15] as arguments.
// You must store the result in a register (R[0]..R[15]).
func WRITE(offset, value int64) { ram[handleoverflow(offset)] = value }

// GET a byte from stdin and return it as an integer.
// The byte is -1 if there was nothing to read (but there might be more later).
// The byte is -2 on EOF.
func GET() int64 {
	n, err := stdin.ReadByte()
	if errors.Is(err, io.EOF) {
		return -2
	}
	if err != nil {
		return -1
	}
	return int64(n)
}

// Exit the program with the given status code. 0 means success.
// Non-0 status codes are implementation-defined, but usually indicate an error.
func EXIT(code int64) {
	os.Exit(int(code))
}

// PUT does I/O by writing a byte to stdout. B must be in the range 0..255: out-of-range values are undefined behavior.
func PUT(b int64) {
	b = handleoverflow(b)
	os.Stdout.Write([]byte{byte(b & 0xFF)}) /* no buffering: we don't care about performance*/
}
